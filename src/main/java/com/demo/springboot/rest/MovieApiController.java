package com.demo.springboot.rest;

import com.demo.springboot.domain.dto.MovieDto;
import com.demo.springboot.domain.dto.MovieListDto;
import com.demo.springboot.service.CsvReaderService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
public class MovieApiController {

    private final CsvReaderService csvReaderService;
    private static final Logger LOGGER = LoggerFactory.getLogger(MovieApiController.class);

    @Autowired
    public MovieApiController(CsvReaderService csvReaderService) {
        this.csvReaderService = csvReaderService;
    }

    @RequestMapping(value = "/movies")
    @ResponseBody
    @CrossOrigin
    public MovieListDto getMovies() {

        LOGGER.info("--- get movies");
        return csvReaderService.readFromCsv();

    }

    @PostMapping("/movies")
    @ResponseBody
    @CrossOrigin
    public ResponseEntity createMovie(@RequestBody MovieDto movieDto) {

        LOGGER.info("--- create movie");
        return csvReaderService.saveToCsv(movieDto);

    }
}
