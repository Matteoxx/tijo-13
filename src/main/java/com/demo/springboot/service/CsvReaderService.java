package com.demo.springboot.service;

import com.demo.springboot.domain.dto.MovieDto;
import com.demo.springboot.domain.dto.MovieListDto;
import org.springframework.http.ResponseEntity;

public interface CsvReaderService {
    MovieListDto readFromCsv();
    ResponseEntity saveToCsv(MovieDto movieDto);
}
