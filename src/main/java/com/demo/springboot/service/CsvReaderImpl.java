package com.demo.springboot.service;

import com.demo.springboot.domain.dto.MovieDto;
import com.demo.springboot.domain.dto.MovieListDto;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
public class CsvReaderImpl implements CsvReaderService {

        @Override
        public MovieListDto readFromCsv(){
            List<MovieDto> movies = new ArrayList<>();
            try (BufferedReader br = new BufferedReader(new FileReader("movies.csv"))) {
                    String line;
                int index = 0;
                while ((line = br.readLine()) != null) {
                    if(index == 0){
                        index++;
                        continue;
                    }

                    String[] values = line.split(";");
                    MovieDto movieDto = new MovieDto(
                            Integer.parseInt(values[0]),
                            values[1].trim(),
                            Integer.parseInt(values[2].trim()),
                            values[3].trim()
                    );
                    movies.add(movieDto);

                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            return new MovieListDto(movies);

        }

        @Override
        public ResponseEntity saveToCsv(MovieDto movieDto){

            if(checkMovieExistence(movieDto.getMovieId())){
                return new ResponseEntity(HttpStatus.CONFLICT);
            }

            try (FileWriter writer = new FileWriter("movies.csv", true)) {

                StringBuilder sb = new StringBuilder();
                sb.append(movieDto.getMovieId());
                sb.append(';');
                sb.append(movieDto.getTitle());
                sb.append(';');
                sb.append(movieDto.getYear());
                sb.append(';');
                sb.append(movieDto.getImage());
                sb.append(';');
                sb.append('\n');
                writer.append(sb);

            } catch (IOException e) {
                e.printStackTrace();
            }
            return new ResponseEntity(HttpStatus.CREATED);
        }

        private boolean checkMovieExistence(int movieId){

            try (BufferedReader br = new BufferedReader(new FileReader("movies.csv"))) {
                String line;
                int index = 0;
                while ((line = br.readLine()) != null) {
                    if(index == 0){
                        index++;
                        continue;
                    }

                    String[] values = line.split(";");
                    if(Integer.parseInt(values[0]) == movieId){
                        return true;
                    }

                }

            } catch (IOException e) {
                e.printStackTrace();
            }
            return false;

        }

}


